<?php

namespace Krak\Func;

function identity($val)
{
    return $val;
}

function map($collection, callable $predicate)
{
    $mapped = [];
    foreach ($collection as $key => $val) {
        $mapped[] = $predicate($val, $key);
    }

    return $mapped;
}

function reduce($collection, callable $predicate, $accumulator = null)
{
    $count = count($collection);
    $i = 0;

    if ($accumulator === null) {
        $accumulator = $collection[0];
        next($collection);
        $i++;
    }

    for ($i; $i < $count; $i++) {
        $accumulator = $predicate($accumulator, current($collection), key($collection), $collection);
        next($collection);
    }

    return $accumulator;
}
