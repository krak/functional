# Function

The function library is just a simple collection of general functional functions.

## API

    function identity($val) -> mixed
    function map($collection, callable $predicate) -> array
    function reduce($collection, callable $predicate, $accumulator = null) -> mixed
