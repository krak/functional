<?php

namespace Krak\Func\Tests;

use Krak\Func;

class FunctionTest extends \PHPUnit_Framework_TestCase
{
    public function testIdentity()
    {
        $this->assertEquals('abc', func\identity('abc'));
    }

    public function testMap()
    {
        $this->assertEquals([1,2], func\map([1,2], 'krak\func\identity'));
    }

    public function testReduceInitial()
    {
        $this->assertEquals(3, func\reduce([1,2], function($total, $val) {
            return $total + $val;
        }));
    }

    public function testReduce()
    {
        $this->assertEquals(5, func\reduce([1, 2], function($total, $val, $idx) {
            return $total + $val + $idx;
        }, 1));
    }
}
